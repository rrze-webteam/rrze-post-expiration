��            )         �     �     �     �     �  
   �     �       $   $     I  O   Q     �     �     �     �     �     �               #     =  *   M  V   x  b   �  9   2     l     x  .   �      �  <   �       �        �  
   �     �     �     �      �  $     (   D  
   m  U   x     �     �     �     	  	   	     &	     :	     O	     \	  %   z	  -   �	  k   �	  w   :
  @   �
     �
       L         e  <   �  
   �                                                          
                                                                	              Content Types Enable Enable expiration Every two minutes Expiration Expiration <b>disabled</b> Expiration Settings Expiration date for posts and pages. Expired Expired <span class="count">(%s)</span> Expired <span class="count">(%s)</span> Expired on: Expired on: <b>%s</b> Expires on: Expires on: <b>%s</b> General Plugins: %1$s: %2$s RRZE Post Expiration RRZE-Webteam Settings issues detected. Settings saved. The Plugin is not network wide compatible. The server is running PHP version %1$s. The Plugin requires at least PHP version %2$s. The server is running WordPress version %1$s. The Plugin requires at least WordPress version %2$s. Type of document to which an expiry date can be assigned. Unnamed tab Y/m/d g:i:s a You do not have enough permissions to do that. https://blogs.fau.de/webworking/ https://gitlab.rrze.fau.de/rrze-webteam/rrze-post-expiration postExpired Project-Id-Version: RRZE Post Expiration
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/rrze-post-expiration
PO-Revision-Date: 2024-05-24 15:19+0200
Last-Translator: RRZE Webteam <webmaster@fau.de>
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 3.4.4
 Inhaltstypen Aktivieren Verfallsdatum aktivieren Alle zwei Minuten Verfallsdatum Verfallsdatum <b>deaktiviert</b> Einstellungen &rsaquo; Verfallsdatum Verfallsdatum für Beiträge und Seiten. Abgelaufen Abgelaufen <span class="count">(%s)</span> Abgelaufen <span class="count">(%s)</span> Abgelaufen am: Abgelaufen am: <b>%s</b> Verfällt am: Verfällt am: <b>%s</b> Allgemein Plugins: %1$s: %2$s RRZE Post Expiration RRZE-Webteam Einstellungsprobleme erkannt. Die Einstellungen wurden gespeichert. Das Plugin ist nicht netzwerkweit kompatibel. Auf dem Server wird die PHP-Version %1$s ausgeführt. Das Plugin erfordert mindestens die PHP-Version %2$s. Auf dem Server wird die WordPress-Version %1$s ausgeführt. Das Plugin erfordert mindestens die WordPress-Version %2$s. Art des Dokuments, dem ein Verfallsdatum zugeordnet werden kann. Unbenannte Registerkarte d.m.Y H:i:s Sie verfügen nicht über die erforderlichen Berechtigungen, um dies zu tun. https://blogs.fau.de/webworking/ https://gitlab.rrze.fau.de/rrze-webteam/rrze-post-expiration Abgelaufen 