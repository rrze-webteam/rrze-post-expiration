<?php

namespace RRZE\PostExpiration;

defined('ABSPATH') || exit;

class Update
{
    const VERSION_OPTION_NAME = 'rrze_post_expiration_version';

    public static function init()
    {
        $version = get_option(self::VERSION_OPTION_NAME, '0');

        if (version_compare($version, '1.4.0', '<')) {
            self::updateExpiredPosts();
            update_option(self::VERSION_OPTION_NAME, '1.4.0');
        }
    }

    protected static function updateExpiredPosts()
    {
        global $wpdb;

        $current_date = current_time('mysql');

        $result = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT post_id FROM {$wpdb->postmeta} as postmeta
                LEFT JOIN {$wpdb->posts} as posts ON postmeta.post_id = posts.ID
                WHERE posts.post_status = 'draft' AND (posts.post_type = 'post' OR posts.post_type = 'page')
                AND postmeta.meta_key = 'expiration_date'
                AND postmeta.meta_value < %s",
                $current_date
            )
        );

        if (!empty($result)) {
            foreach ($result as $row) {
                wp_update_post(['ID' => $row->post_id, 'post_status' => 'expired']);
                // rrze cache plugin support
                Utils::flushCacheUrl(get_permalink($row->post_id));
            }
        }
    }
}
