<?php

namespace RRZE\PostExpiration;

defined('ABSPATH') || exit;

class Metabox
{
    public static function init()
    {
        add_action('post_submitbox_misc_actions', [__CLASS__, 'expMetaBox']);
    }

    public static function expMetaBox()
    {
        global $post, $post_type, $post_type_object;

        if (!in_array($post_type, settings()->getOption('post_types'))) {
            return;
        }

        if (!current_user_can($post_type_object->cap->publish_posts)) {
            return;
        }

        /* translators: Publish box date string. 1: Date, 2: Time. */
        $date_string = __('%1$s at %2$s');
        /* translators: Publish box date format. */
        $dateFormat = _x('M j, Y', 'publish box date format');
        /* translators: Publish box time format. */
        $timeFormat = _x('H:i', 'publish box time format');
        $currentTimestamp = current_time('timestamp');
        $date = sprintf(
            $date_string,
            date_i18n($dateFormat, $currentTimestamp),
            date_i18n($timeFormat, $currentTimestamp)
        );
        $stamp = __('Expiration <b>disabled</b>', 'rrze-post-expiration');
        if (0 != $post->ID) {
            $expirationDate = get_post_meta($post->ID, Post::EXPIRATION_DATE_META_KEY, true);
            if ($expirationDate) {
                $expirationTimestamp = strtotime($expirationDate);
                if ($expirationTimestamp > $currentTimestamp) {
                    $stamp = __(
                        /* translators: %s: Expiration date. */
                        'Expires on: <b>%s</b>',
                        'rrze-post-expiration'
                    );
                } else {
                    $stamp = __(
                        /* translators: %s: Expiration date. */
                        'Expired on: <b>%s</b>',
                        'rrze-post-expiration'
                    );
                }
                $date = sprintf(
                    $date_string,
                    date_i18n($dateFormat, strtotime($expirationDate)),
                    date_i18n($timeFormat, strtotime($expirationDate))
                );
            }
        } ?>
        <div class="misc-pub-section curtime misc-pub-curtime">
            <span id="timestamp" class="exp_timestamp">
                <?php printf($stamp, $date); ?></span>
            <a href="#edit_exp_timestamp" class="edit-exp-timestamp hide-if-no-js" role="button">
                <span aria-hidden="true"><?php _e('Edit') ?></span>
                <span class="screen-reader-text"><?php _e('Edit date and time') ?></span>
            </a>
            <fieldset id="exp_timestampdiv" class="hide-if-js">
                <legend class="screen-reader-text"><?php _e('Date and time') ?></legend>
                <div class="timestamp-wrap">
                    <?php self::selectExpTime(); ?>
                </div>
            </fieldset>
        </div>
<?php
        wp_nonce_field('save_post_expiration_meta', 'post_expiration_nonce');
    }

    private static function selectExpTime()
    {
        global $wp_locale, $post;

        $expirationDate = get_post_meta($post->ID, 'expiration_date', true);

        $enabled = '';
        if ($expirationDate) {
            $enabled = ' checked="checked"';
        }

        $edit = !(!$expirationDate);

        $currentTimestamp = current_time('timestamp');

        $jj = ($edit) ? mysql2date('d', $expirationDate, false) : gmdate('d', $currentTimestamp);
        $mm = ($edit) ? mysql2date('m', $expirationDate, false) : gmdate('m', $currentTimestamp);
        $aa = ($edit) ? mysql2date('Y', $expirationDate, false) : gmdate('Y', $currentTimestamp);
        $hh = ($edit) ? mysql2date('H', $expirationDate, false) : gmdate('H', $currentTimestamp);
        $mn = ($edit) ? mysql2date('i', $expirationDate, false) : gmdate('i', $currentTimestamp);
        $ss = ($edit) ? mysql2date('s', $expirationDate, false) : gmdate('s', $currentTimestamp);

        $cur_jj = gmdate('d', $currentTimestamp);
        $cur_mm = gmdate('m', $currentTimestamp);
        $cur_aa = gmdate('Y', $currentTimestamp);
        $cur_hh = gmdate('H', $currentTimestamp);
        $cur_mn = gmdate('i', $currentTimestamp);

        $month = '<select class="form-required" id="exp_mm" name="exp_mm">' . PHP_EOL;
        $month .= '<label><span class="screen-reader-text">' . __('Month') . '</span>';
        for ($i = 1; $i < 13; $i = $i + 1) {
            $monthnum = zeroise($i, 2);
            $monthtext = $wp_locale->get_month_abbrev($wp_locale->get_month($i));
            $month .= "\t\t\t" . '<option value="' . $monthnum . '" data-text="' . $monthtext . '" ' . selected($monthnum, $mm, false) . '>';
            /* translators: 1: month number (01, 02, etc.), 2: month abbreviation */
            $month .= sprintf(__('%1$s-%2$s'), $monthnum, $monthtext) . '</option>' . PHP_EOL;
        }
        $month .= '</select></label>';

        $day = '<label><span class="screen-reader-text">' . __('Day') . '</span>';
        $day .= '<input type="text" id="exp_jj" name="exp_jj" value="' . $jj . '" size="2" maxlength="2" autocomplete="off" class="form-required" />';
        $day .= '</label>';

        $year = '<label><span class="screen-reader-text">' . __('Year') . '</span>';
        $year .= '<input type="text" id="exp_aa" name="exp_aa" value="' . $aa . '" size="4" maxlength="4" autocomplete="off" class="form-required" />';
        $year .= '</label>';

        $hour = '<label><span class="screen-reader-text">' . __('Hour') . '</span>';
        $hour .= '<input type="text" id="exp_hh" name="exp_hh" value="' . $hh . '" size="2" maxlength="2" autocomplete="off" class="form-required" />';
        $hour .= '</label>';

        $minute = '<label><span class="screen-reader-text">' . __('Minute') . '</span>';
        $minute .= '<input type="text" id="exp_mn" name="exp_mn" value="' . $mn . '" size="2" maxlength="2" autocomplete="off" class="form-required" />';
        $minute .= '</label>';

        echo '<input type="hidden" id="exp_ss" name="exp_ss" value="' . $ss . '" />';

        echo '<p style="margin:0.5em 0;"><input type="checkbox" name="enable_expiration" id="enable_expiration" value="checked"' . $enabled . ' />';
        echo '<label class="selectit" for="enable_expiration">' . __('Enable expiration', 'rrze-post-expiration') . '</label></p>';

        /* translators: 1: month, 2: day, 3: year, 4: hour, 5: minute */
        printf(__('%1$s %2$s, %3$s at %4$s:%5$s'), $month, $day, $year, $hour, $minute);
        echo PHP_EOL;

        if ($enabled) {
            echo '<input type="hidden" id="exp_enabled" name="exp_enabled" value="1" />';
        } else {
            echo '<input type="hidden" id="exp_enabled" name="exp_enabled" value="0" />';
        }
        echo PHP_EOL;

        foreach (['mm', 'jj', 'aa', 'hh', 'mn'] as $timeUnit) {
            echo '<input type="hidden" id="exp_hidden_' . $timeUnit . '" name="hidden_' . $timeUnit . '" value="' . $$timeUnit . '" />' . "\n";
            $currentTimeUnit = 'cur_' . $timeUnit;
            echo '<input type="hidden" id="exp_' . $currentTimeUnit . '" name="exp_' . $currentTimeUnit . '" value="' . $$currentTimeUnit . '" />' . "\n";
        }

        echo '<p>';
        echo '<a href="#edit_exp_timestamp" class="save-exp-timestamp hide-if-no-js button">', __('OK'), '</a>';
        echo '<a href="#edit_exp_timestamp" class="cancel-exp-timestamp hide-if-no-js">', __('Cancel'), '</a>';
        echo '</p>';
    }
}
