<?php

namespace RRZE\PostExpiration;

defined('ABSPATH') || exit;

class Options
{
    /**
     * Option name
     * @var string
     */
    protected static $optionName = 'rrze_post_expiration';

    /**
     * Default options
     * @return array
     */
    protected static function defaultOptions(): array
    {
        $options = [
            'post_types' => ['post']
        ];

        return $options;
    }

    /**
     * Returns the options.
     * @return object
     */
    public static function getOptions(): object
    {
        $defaults = self::defaultOptions();

        $options = (array) get_option(self::$optionName);
        $options = wp_parse_args($options, $defaults);
        $options = array_intersect_key($options, $defaults);

        return (object) $options;
    }

    /**
     * Returns the name of the option.
     * @return string
     */
    public static function getOptionName(): string
    {
        return self::$optionName;
    }
}

