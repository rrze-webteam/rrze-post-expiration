<?php

namespace RRZE\PostExpiration;

defined('ABSPATH') || exit;

class Main
{
    public function __construct()
    {
        settings()->loaded();

        $availablePostTypes = settings()->getOption('post_types');
        if (empty($availablePostTypes)) {
            return;
        }

        add_filter('plugin_action_links_' . plugin()->getBaseName(), [$this, 'settingsLink']);

        Post::init();

        add_action('admin_init', function () {
            $screen = get_current_screen();
            if (!$screen || !method_exists($screen, 'is_block_editor')) {
                Metabox::init();
                // Enqueue Classic Editor Assets
                add_action('admin_enqueue_scripts', [$this, 'adminEnqueueScripts']);
            }
        });

        // Enqueue Block Editor Assets
        add_action('enqueue_block_editor_assets', [$this, 'enqueueBlockEditorAssets']);

        Update::init();

        Cron::init();
    }

    public function __clone()
    {
        trigger_error('Cloning is not allowed.', E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error('Unserialize is forbidden.', E_USER_ERROR);
    }

    /**
     * Add the settings link to the list of plugins.
     *
     * @param array $links
     * @return void
     */
    public function settingsLink($links)
    {
        $settingsLink = sprintf(
            '<a href="%s">%s</a>',
            admin_url('options-general.php?page=rrze_post_expiration'),
            __('Settings', 'rrze_post_expiration')
        );
        array_unshift($links, $settingsLink);
        return $links;
    }

    public function enqueueBlockEditorAssets()
    {
        $assetFile = include plugin()->getPath('build') . 'block-editor.asset.php';
        wp_enqueue_script(
            'post-expiration-block-editor',
            plugins_url('build/block-editor.js', plugin()->getBasename()),
            $assetFile['dependencies'] ?? [],
            $assetFile['version'],
            true
        );

        wp_set_script_translations(
            'post-expiration-block-editor',
            'rrze-post-expiration',
            plugin()->getPath('languages')
        );
    }

    public function adminEnqueueScripts($hook)
    {
        if ($hook !== 'post.php' && $hook !== 'post-new.php' && !in_array(get_post_type(), ['post', 'page'])) {
            return;
        }

        wp_enqueue_style(
            'rrze-post-expiration-classic-editor',
            plugins_url('build/classic-editor.css', plugin()->getBasename()),
            [],
            plugin()->getVersion()
        );

        $assetFile = include plugin()->getPath('build') . 'classic-editor.asset.php';
        wp_enqueue_script(
            'rrze-post-expiration-classic-editor',
            plugins_url('build/classic-editor.js', plugin()->getBasename()),
            $assetFile['dependencies'] ?? [],
            plugin()->getVersion()
        );

        wp_localize_script(
            'rrze-post-expiration-classic-editor',
            'expiration',
            [
                'expiresOn' => __('Expires on:', 'rrze-post-expiration'),
                'expiredOn' => __('Expired on:', 'rrze-post-expiration'),
                'expirationDate' => __('Expiration <b>disabled</b>', 'rrze-post-expiration')
            ]
        );
    }
}
