<?php

namespace RRZE\PostExpiration;

defined('ABSPATH') || exit;

class Utils
{
    /**
     * Check if the screen is a block editor screen.
     * @return bool
     */
    public static function isBlockEditorScreen()
    {
        $screen = get_current_screen();
        if ($screen && method_exists($screen, 'is_block_editor')) {
            return $screen->is_block_editor();
        }
        return false;
    }

    /**
     * Flush the Cache for an Url.
     * RRZE Cache plugin support.
     * @param  string $url Post permalink
     */
    public static function flushCacheUrl(string $url)
    {
        if (!empty($url) && has_action('rrzecache_flush_cache_url')) {
            do_action('rrzecache_flush_cache_url', $url);
        }
    }
}
