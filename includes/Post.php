<?php

namespace RRZE\PostExpiration;

defined('ABSPATH') || exit;

class Post
{
    const EXPIRED_POST_STATUS = 'expired';

    const EXPIRATION_DATE_META_KEY = 'expiration_date';

    const EXPIRATION_DATE_GMT_META_KEY = 'expiration_date_gmt';

    const EXPIRATION_ENABLED_META_KEY = 'expiration_enabled';

    public static function init()
    {
        foreach (settings()->getOption('post_types') as $postType) {
            add_filter('manage_' . $postType . '_posts_columns', [__CLASS__, 'expDateColumn']);
            add_action('manage_' . $postType . '_posts_custom_column', [__CLASS__, 'expDateCustomColumn'], 10, 2);
            add_filter('manage_edit-' . $postType . '_sortable_columns', [__CLASS__, 'expDateSortableColumn']);
        }

        add_action('init', [__CLASS__, 'registerMetas']);

        add_action('init', [__CLASS__, 'registerPostStatus']);
        add_action('admin_footer-post.php', [__CLASS__, 'appendPostStatusPostbox']);
        add_action('admin_footer-edit.php', [__CLASS__, 'appendPostStatusQuickEdit']);

        add_action('pre_get_posts', [__CLASS__, 'expDateOrderby']);

        add_action('save_post', [__CLASS__, 'savePost'], 10, 2);
        add_action('updated_postmeta', [__CLASS__, 'updatedPostMeta'], 10, 4);
    }

    public static function registerMetas()
    {
        foreach (settings()->getOption('post_types') as $postType) {
            register_meta(
                'post',
                self::EXPIRATION_DATE_META_KEY,
                [
                    'show_in_rest' => true,
                    'single'       => true,
                    'type'         => 'string',
                    'object_subtype' => $postType,
                ]
            );
            register_meta(
                'post',
                self::EXPIRATION_DATE_GMT_META_KEY,
                [
                    'show_in_rest' => true,
                    'single'       => true,
                    'type'         => 'string',
                    'object_subtype' => $postType,
                ]
            );
            register_meta(
                'post',
                self::EXPIRATION_ENABLED_META_KEY,
                [
                    'show_in_rest' => true,
                    'single' => true,
                    'type' => 'boolean',
                    'object_subtype' => $postType,
                ]
            );
        }
    }

    public static function expDateColumn($columns)
    {
        $columns['expiration_date'] = __('Expiration', 'rrze-post-expiration');
        return $columns;
    }

    public static function expDateCustomColumn($column, $postId)
    {
        if ($column != 'expiration_date') {
            return;
        }

        $post = get_post($postId);

        $expirationEnabled = get_post_meta($post->ID, self::EXPIRATION_ENABLED_META_KEY, true);
        $expirationDate = get_post_meta($post->ID, self::EXPIRATION_DATE_META_KEY, true);
        $expirationDateGmt = get_post_meta($post->ID, self::EXPIRATION_DATE_GMT_META_KEY, true);

        if ($post->post_date == '0000-00-00 00:00:00' || !$expirationDate || !$expirationEnabled) {
            echo '&#8212;';
        } else {
            $datetime = date(__('Y/m/d g:i:s a', 'rrze-post-expiration'), mysql2date('U', $expirationDate));
            $timestamp = $expirationDate;

            $timestampGtm = mysql2date('G', $expirationDateGmt);

            $timeDiff = $timestampGtm - time();

            if ($timeDiff > 0 && $timeDiff < DAY_IN_SECONDS) {
                $humanTime = sprintf(__('in %s'), human_time_diff($timestampGtm));
            } elseif ($timeDiff < 0) {
                $humanTime = sprintf(__('%s ago'), human_time_diff($timestampGtm));
            } else {
                $humanTime = '';
            }

            if ($timeDiff < 0) {
                echo '<span class="expired">' . __('Expired', 'rrze-post-expiration') . '</span>';
                $humanTimeSpan = '<abbr class="expired" title="' . $datetime . '">' . $humanTime . '</abbr>';
            } else {
                echo '<abbr title="' . $datetime . '">' . mysql2date(__('Y/m/d'), $timestamp) . '</abbr>';
                $humanTimeSpan = $humanTime ? '(' . $humanTime . ')' : '';
            }

            echo '<br/>';
            echo $humanTimeSpan;
        }
    }

    public static function expDateSortableColumn($columns)
    {
        $columns['expiration_date'] = 'expiration_date';
        return $columns;
    }

    public static function expDateOrderby($query)
    {
        if (!is_admin()) {
            return;
        }

        $orderby = $query->get('orderby');
        if ('expiration_date' == $orderby) {
            $query->set('orderby', 'meta_value');
            $query->set(
                'meta_query',
                [
                    'relation' => 'OR',
                    [
                        'key' => self::EXPIRATION_DATE_META_KEY,
                        'compare' => 'NOT EXISTS'
                    ],
                    [
                        'key' => self::EXPIRATION_DATE_META_KEY,
                        'compare' => 'EXISTS'
                    ],
                ]
            );
        }
    }

    public static function registerPostStatus()
    {
        register_post_status(self::EXPIRED_POST_STATUS, [
            'label'                     => _x('Expired', 'post', 'rrze-post-expiration'),
            'public'                    => false,
            'private'                   => false,
            'exclude_from_search'       => true,
            'show_in_admin_all_list'    => false,
            'show_in_admin_status_list' => true,
            'label_count'               => _n_noop(
                /* translators: %s: Number of expired posts. */
                'Expired <span class="count">(%s)</span>',
                'Expired <span class="count">(%s)</span>',
                'rrze-post-expiration'
            )
        ]);
    }

    public static function appendPostStatusPostbox()
    {
        $post = get_post();
        if (!$post || !in_array($post->post_type, settings()->getOption('post_types'))) {
            return;
        }

        $selected = '';
        $postStatusDisplay = '';
        $title = __('Expired', 'rrze-post-expiration');
        if ($post->post_status == self::EXPIRED_POST_STATUS) {
            $postStatusDisplay = '$(".misc-pub-post-status span#post-status-display").text("' . $title . '");';
            $selected = ' selected=\"selected\"';
        }
        $script = '
        <script>
        jQuery(document).ready(function($){
        $("select#post_status").append("<option value=\"expired\"' . $selected . '>' . $title . '</option>");
        ' . $postStatusDisplay . '
        });
        </script>
        ';
        echo str_replace(PHP_EOL, '', trim(preg_replace('/\s+/', ' ', $script)));
    }

    public static function appendPostStatusQuickEdit()
    {
        $post = get_post();
        if (!$post || !in_array(get_post_type(), settings()->getOption('post_types'))) {
            return;
        }

        $title = __('Expired', 'rrze-post-expiration');
        $script = "<script>
        jQuery(document).ready(function($) {
        $('select[name=\"_status\"]').append('<option value=\"expired\">" . $title . "</option>');
        });
        </script>";
        echo str_replace(PHP_EOL, '', trim(preg_replace('/\s+/', ' ', $script)));
    }

    public static function savePost($postId, $post)
    {
        // Check if it is not saving automatically
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        // Check permissions
        if (!current_user_can('edit_post', $postId)) {
            return;
        }

        // Check if all the values are there
        if (
            !isset($_POST['post_expiration_nonce'])
            || !isset($_POST['exp_aa'])
            || !isset($_POST['exp_mm'])
            || !isset($_POST['exp_jj'])
            || !isset($_POST['exp_hh'])
            || !isset($_POST['exp_mn'])
            || !isset($_POST['exp_ss'])
        ) {
            return;
        }

        if (!in_array($post->post_type, settings()->getOption('post_types'))) {
            return;
        }

        // Check the nonce
        check_admin_referer('save_post_expiration_meta', 'post_expiration_nonce');

        delete_post_meta($postId, self::EXPIRATION_ENABLED_META_KEY);
        delete_post_meta($postId, self::EXPIRATION_DATE_META_KEY);
        delete_post_meta($postId, self::EXPIRATION_DATE_GMT_META_KEY);

        if (isset($_POST['enable_expiration'])) {
            $aa = $_POST['exp_aa'];
            $mm = $_POST['exp_mm'];
            $jj = $_POST['exp_jj'];
            $hh = $_POST['exp_hh'];
            $mn = $_POST['exp_mn'];
            $ss = $_POST['exp_ss'];
            $aa = ($aa <= 0) ? date('Y') : $aa;
            $mm = ($mm <= 0) ? date('n') : $mm;
            $jj = ($jj > 31) ? 31 : $jj;
            $jj = ($jj <= 0) ? date('j') : $jj;
            $hh = ($hh > 23) ? $hh - 24 : $hh;
            $mn = ($mn > 59) ? $mn - 60 : $mn;
            $ss = ($ss > 59) ? $ss - 60 : $ss;

            $timestamp = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $aa, $mm, $jj, $hh, $mn, $ss);
            $updateExpirationDate = update_post_meta($postId, self::EXPIRATION_DATE_META_KEY, $timestamp, true);

            $timestampGmt = get_gmt_from_date($timestamp);
            $updateExpirationDateGMT = update_post_meta($postId, self::EXPIRATION_DATE_GMT_META_KEY, $timestampGmt, true);

            if ($updateExpirationDate !== false && $updateExpirationDateGMT !== false) {
                update_post_meta($postId, self::EXPIRATION_ENABLED_META_KEY, true, true);
            }
        }
    }

    public static function updatedPostMeta($metaId, $postId, $metaKey, $metaValue)
    {
        if (self::EXPIRATION_ENABLED_META_KEY == $metaKey && !$metaValue) {
            delete_post_meta($postId, self::EXPIRATION_ENABLED_META_KEY);
            delete_post_meta($postId, self::EXPIRATION_DATE_META_KEY);
            delete_post_meta($postId, self::EXPIRATION_DATE_GMT_META_KEY);
        }
    }
}
