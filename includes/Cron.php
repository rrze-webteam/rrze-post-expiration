<?php

namespace RRZE\PostExpiration;

defined('ABSPATH') || exit;

class Cron
{
    const EXPIRATION_SCHEDULE_HOOK = 'rrze_post_expiration_every2minutes_event';

    const EXPIRATION_CUSTOM_CRON_SCHEDULE = 'rrze_post_expiration_every2minutes';

    public static function init()
    {
        add_action('init', [__CLASS__, 'activateScheduledEvents']);
        add_filter('cron_schedules', [__CLASS__, 'addCustomCronSchedule']);
        add_action(self::EXPIRATION_SCHEDULE_HOOK, [__CLASS__, 'every2MinutesEvent']);
    }

    /**
     * Activate the scheduled events.
     * @return void
     */
    public static function activateScheduledEvents()
    {
        if (!wp_next_scheduled(self::EXPIRATION_SCHEDULE_HOOK)) {
            wp_schedule_event(time(), self::EXPIRATION_CUSTOM_CRON_SCHEDULE, self::EXPIRATION_SCHEDULE_HOOK);
        }
    }

    /**
     * Run the event every 2 minutes.
     * @return void
     */
    public static function every2MinutesEvent()
    {
        $availablePostTypes = settings()->getOption('post_types');
        if (empty($availablePostTypes) || !is_array($availablePostTypes)) {
            return;
        }

        $args = [
            'post_type'      => $availablePostTypes,
            'post_status'    => 'publish',
            'fields'         => 'ids', // Get only the post IDs to improve performance
            'posts_per_page' => -1, // Get all matching posts
            'meta_query'     => [
                'relation' => 'AND',
                [
                    'key'   => Post::EXPIRATION_ENABLED_META_KEY,
                    'value' => '1',
                    'compare' => '='
                ],
                [
                    'key'     => Post::EXPIRATION_DATE_META_KEY,
                    'value'   => current_time('mysql'),
                    'compare' => '<=',
                    'type'    => 'DATETIME'
                ]
            ]
        ];

        $posts = get_posts($args);

        if (!empty($posts)) {
            foreach ($posts as $postId) {
                wp_update_post(['ID' => $postId, 'post_status' => Post::EXPIRED_POST_STATUS]);
                // RRZE Cache plugin support
                Utils::flushCacheUrl(get_permalink($postId));
            }
        }
    }

    /**
     * Add a custom cron schedule.
     * @param array $schedules Available cron schedules
     * @return array New cron schedules
     */
    public static function addCustomCronSchedule(array $schedules): array
    {
        $schedules[self::EXPIRATION_CUSTOM_CRON_SCHEDULE] = [
            'interval' => 2 * MINUTE_IN_SECONDS,
            'display' => __('Every two minutes', 'rrze-post-expiration')
        ];
        return $schedules;
    }

    /**
     * Clear the schedule.
     * @return void
     */
    public static function clearScheduleHook()
    {
        wp_clear_scheduled_hook(self::EXPIRATION_SCHEDULE_HOOK);
    }

    /**
     * Remove the custom cron schedule.
     * @return void
     */
    public static function removeCustomCronSchedule()
    {
        add_filter('cron_schedules', function ($schedules) {
            unset($schedules[self::EXPIRATION_CUSTOM_CRON_SCHEDULE]);
            return $schedules;
        });
    }
}
