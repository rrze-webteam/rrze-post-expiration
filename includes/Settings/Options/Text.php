<?php

namespace RRZE\PostExpiration\Settings\Options;

defined('ABSPATH') || exit;

class Text extends Type
{
    public $template = 'text';
}
