<?php

namespace RRZE\PostExpiration\Settings\Options;

defined('ABSPATH') || exit;

class Select extends Type
{
    public $template = 'select';
}
