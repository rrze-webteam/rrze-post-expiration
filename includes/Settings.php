<?php

namespace RRZE\PostExpiration;

defined('ABSPATH') || exit;

use RRZE\PostExpiration\Settings\Settings as OptionsSettings;

class Settings
{
    const OPTION_NAME = 'rrze_post_expiration';

    protected $settings;

    public function __construct()
    {
        // Nothing to do here.
    }

    public function loaded()
    {
        $this->settings = new OptionsSettings(__('Expiration Settings', 'rrze-post-expiration'), 'rrze_post_expiration');

        $this->settings->setCapability('manage_options')
            ->setOptionName(self::OPTION_NAME)
            ->setMenuTitle(__('Expiration', 'rrze-post-expiration'))
            ->setMenuPosition(6)
            ->setMenuParentSlug('options-general.php');

        $section = $this->settings->addSection(
            __('General', 'rrze-post-expiration')
        );

        $section->addOption('checkbox-multiple', [
            'name' => 'post_types',
            'label' => __('Content Types', 'rrze-post-expiration'),
            'description' => __('Type of document to which an expiry date can be assigned.', 'rrze-post-expiration'),
            'options' => [
                'post' => $this->postTypeLabel('post'),
                'page' => $this->postTypeLabel('page')
            ],
            'default' => ['post']
        ]);

        $this->settings->build();
    }

    protected function postTypeLabel($postType = 'post')
    {
        $postTypes = get_post_types(['show_in_menu' => true], 'objects');

        $labels = wp_list_pluck($postTypes, 'labels');
        $labels = wp_list_pluck($labels, 'name');

        return isset($labels[$postType]) ?  $labels[$postType] : $postType;
    }

    public function getOption($option)
    {
        return $this->settings->getOption($option);
    }

    public function getOptions()
    {
        return $this->settings->getOptions();
    }
}
