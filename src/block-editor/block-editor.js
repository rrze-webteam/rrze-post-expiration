/**
 * WordPress dependencies.
 */
import { __ } from "@wordpress/i18n";
import { TimePicker, ToggleControl } from "@wordpress/components";
import { PluginDocumentSettingPanel } from "@wordpress/edit-post";
import { registerPlugin } from "@wordpress/plugins";
import { useState } from "@wordpress/element";
import { useSelect, useDispatch } from "@wordpress/data";

const PostExpirationComponent = () => {
    const { editPost } = useDispatch("core/editor");
    const meta = useSelect(
        (select) => select("core/editor").getEditedPostAttribute("meta"),
        []
    );
    const [isEnabled, setIsEnabled] = useState(
        (meta && meta.expiration_enabled) || false
    );
    const [time, setTime] = useState(
        meta && meta.expiration_date !== undefined
            ? meta.expiration_date
            : new Date()
    );
    const handleTimeChange = (newTime) => {
        setTime(newTime);
        const newTimeGMT = new Date(newTime).toISOString().slice(0, -5);
        const newMeta = {
            ...meta,
            expiration_date: newTime,
            expiration_date_gmt: newTimeGMT,
        };
        editPost({ meta: newMeta });
    };
    const handleToggleChange = (newValue) => {
        setIsEnabled(newValue);
        let newMeta = { ...meta, expiration_enabled: newValue };
        if (!newValue) {
            setTime(new Date());
            newMeta = {
                ...newMeta,
                expiration_date: new Date(),
                expiration_date_gmt: new Date().toISOString().slice(0, -5),
            };
        }
        editPost({ meta: newMeta });
    };
    return (
        <PluginDocumentSettingPanel
            name="rrze-post-expiration-block-editor"
            title={__("Expiration", "rrze-post-expiration")}
            className="rrze-post-expiration-block-editor"
        >
            <ToggleControl
                label={__("Enable", "rrze-post-expiration")}
                checked={isEnabled}
                onChange={handleToggleChange}
            />
            {isEnabled && (
                <TimePicker currentTime={time} onChange={handleTimeChange} />
            )}
        </PluginDocumentSettingPanel>
    );
};

registerPlugin("rrze-post-expiration-block-editor", {
    render: PostExpirationComponent,
});
