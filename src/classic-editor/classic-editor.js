jQuery(document).ready(function ($) {
    enabled = $("#exp_enabled").val();

    $("#exp_timestampdiv")
        .siblings("a.edit-exp-timestamp")
        .click(function () {
            if ($("#exp_timestampdiv").is(":hidden")) {
                $("#exp_timestampdiv").slideDown("normal");
                $(this).hide();
            }
            return false;
        });

    $(".cancel-exp-timestamp", "#exp_timestampdiv").click(function () {
        $("#exp_timestampdiv").slideUp("normal");
        $("#exp_mm").val($("#exp_hidden_mm").val());
        $("#exp_jj").val($("#exp_hidden_jj").val());
        $("#exp_aa").val($("#exp_hidden_aa").val());
        $("#exp_hh").val($("#exp_hidden_hh").val());
        $("#exp_mn").val($("#exp_hidden_mn").val());
        $("#exp_timestampdiv").siblings("a.edit-exp-timestamp").show();
        updateExpiration();
        return false;
    });

    $(".save-exp-timestamp", "#exp_timestampdiv").click(function () {
        if (updateExpiration()) {
            $("#exp_timestampdiv").slideUp("normal");
            $("#exp_timestampdiv").siblings("a.edit-exp-timestamp").show();
        }
        return false;
    });

    function updateExpiration() {
        var stamp = $(".exp_timestamp").html(),
            attemptedDate,
            postDate,
            currentDate,
            postStatus = $("#post_status"),
            optPublish = $("option[value=publish]", postStatus),
            aa = $("#exp_aa").val(),
            mm = $("#exp_mm").val(),
            jj = $("#exp_jj").val(),
            hh = $("#exp_hh").val(),
            mn = $("#exp_mn").val(),
            txt = "";

        attemptedDate = new Date(aa, mm - 1, jj, hh, mn);
        postDate = new Date(
            $("#exp_hidden_aa").val(),
            $("#exp_hidden_mm").val() - 1,
            $("#exp_hidden_jj").val(),
            $("#exp_hidden_hh").val(),
            $("#exp_hidden_mn").val()
        );
        currentDate = new Date(
            $("#exp_cur_aa").val(),
            $("#exp_cur_mm").val() - 1,
            $("#exp_cur_jj").val(),
            $("#exp_cur_hh").val(),
            $("#exp_cur_mn").val()
        );

        if (
            attemptedDate.getFullYear() != aa ||
            1 + attemptedDate.getMonth() != mm ||
            attemptedDate.getDate() != jj ||
            attemptedDate.getMinutes() != mn
        ) {
            $(".exp-timestamp-wrap", "#exp_timestampdiv").addClass(
                "form-invalid"
            );
            return false;
        } else {
            $(".exp-timestamp-wrap", "#exp_timestampdiv").removeClass(
                "form-invalid"
            );
        }

        if ($("#enable_expiration").attr("checked") == "checked") {
            if (attemptedDate > currentDate) txt = expiration.expiresOn;
            else txt = expiration.expiredOn;

            if (
                postDate.toUTCString() == attemptedDate.toUTCString() &&
                enabled == 1
            ) {
                $(".exp_timestamp").html(stamp);
            } else {
                $(".exp_timestamp").html(
                    txt +
                        " <b>" +
                        $(
                            "option[value=" + $("#exp_mm").val() + "]",
                            "#exp_mm"
                        ).text() +
                        " " +
                        jj +
                        " " +
                        aa +
                        " at " +
                        hh +
                        ":" +
                        mn +
                        "</b> "
                );
            }
        } else {
            enabled = 0;
            $(".exp_timestamp").html(expiration.expirationDate);
        }

        return true;
    }
});
